require('babel-register')({
    presets: ['react']
});
var express = require('express');

// external open source libraries
const async = require('async');
var React = require('react');
var ReactDOMServer = require('react-dom/server');

//app source code
var Component = require('./Component.jsx');
var questions = require('./questions');
var fetchData = require('./fetchData')

var app = express();

app.get('/', function (req, res, next) {
    var result = "this is home"
    // render the component
    var html = ReactDOMServer.renderToString(
        React.createElement(Component, { answers: JSON.stringify(result) })
    );
    res.send(html);
});

app.get('/evaluation', function (req, res, next) {
    async.map(req.query.url, fetchData.getPoliticalPropogandaData, function (err, respData, headerText) {
        if (err) return console.log(err);

        // concat the response from different url into single response         
        var mergedPropogandaRecords = [].concat.apply([], respData);
        //console.log("mergedPropogandaRecords ", mergedPropogandaRecords) 

        // runtime get questions from the helper. Idea is to keep questions flexible. 
        var initialAnswers = questions.answersFor() // initial answer for all questions is null
        initialAnswers.map(function (data) {
            data.returnVal = questions[data.name](mergedPropogandaRecords, headerText) // Execute all the questions runtime
        })

        // prepare the desired result
        var result = initialAnswers.reduce((acc, o) => (acc[o.name] = o.returnVal, acc), {});
        console.log(JSON.stringify(result));

        // render the component
        var html = ReactDOMServer.renderToString(
            React.createElement(Component, { answers: JSON.stringify(result) })
        );
        res.send(html);
    });
});

var PORT = 3000;
var server = app.listen(PORT, function () {
    console.log('http://localhost:' + PORT);
});
module.exports = server;