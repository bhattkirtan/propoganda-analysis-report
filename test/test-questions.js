var assert = require('assert');
var questions = require('../questions.jsx');

describe('Question', function () {
  describe('#MostSpeeches()', function () {
    it('should return "Alexander Abel" with most occurances', function () {

      const csvData = 
      [ { Redner: 'Alexander Abel', Thema: 'Innere Sicherheit', Datum: '2013-12-11', Wörter: '911' },
      { Redner: 'Caesare Collins', Thema: 'Kohlesubventionen', Datum: '2013-11-06', Wörter: '1119' },
      { Redner: 'Bernhard Belling', Thema: 'Kohlesubventionen', Datum: '2013-11-05', Wörter: '1210' },
      { Redner: 'Alexander Abel', Thema: 'Bildungspolitik', Datum: '2013-10-30', Wörter: '5310' },
      { Redner: 'Alexander Abel',Thema: 'Innere Sicherheit', Datum: '2013-12-11', Wörter: '911' } ]
      assert.equal("Alexander Abel", questions.MostSpeeches(csvData));
    });
  });
});

describe('Question', function () {
    describe('#MostSpeeches()', function () {
      it('should return "Bernhard Belling" with most occurances for year 2013', function () {
  
        const csvData = [ { Redner: 'Alexander Abel', Thema: 'Innere Sicherheit', Datum: '2012-12-11', 'Wörter': '911' },
        { Redner: 'Caesare Collins', Thema: 'Kohlesubventionen', Datum: '2012-11-06', 'Wörter': '1119' },
        { Redner: 'Bernhard Belling', Thema: 'Kohlesubventionen', Datum: '2013-11-05', 'Wörter': '1210' },
        { Redner: 'Alexander Abel', Thema: 'Bildungspolitik', Datum: '2012-10-30', 'Wörter': '5310' },
        { Redner: 'Alexander Abel',Thema: 'Innere Sicherheit', Datum: '2012-12-11', 'Wörter': '911' } ]
  
        assert.equal("Bernhard Belling", questions.MostSpeeches(csvData));
      });
    });
});

describe('Question', function () {
    describe('#MostSpeeches()', function () {
      it('should return "null" as clear answer is not available for most occurances in year 2013', function () {
  
        const csvData = [ { Redner: 'Alexander Abel', Thema: 'Innere Sicherheit', Datum: '2012-12-11', 'Wörter': '911' },
        { Redner: 'Caesare Collins', Thema: 'Kohlesubventionen', Datum: '2012-11-06', 'Wörter': '1119' },
        { Redner: 'Bernhard Belling', Thema: 'Kohlesubventionen', Datum: '2012-11-05', 'Wörter': '1210' },
        { Redner: 'Alexander Abel', Thema: 'Bildungspolitik', Datum: '2012-10-30', 'Wörter': '5310' },
        { Redner: 'Alexander Abel',Thema: 'Innere Sicherheit', Datum: '2012-12-11', 'Wörter': '911' } ]
  
        assert.equal(null, questions.MostSpeeches(csvData));
      });
    });
});

describe('Question', function () {
    describe('#MostSecurity()', function () {
      it('should return "Alexander Abel" with most speeches on Innere Sicherheit', function () {
  
        const csvData = [ { Redner: 'Alexander Abel', Thema: 'Innere Sicherheit', Datum: '2012-12-11', 'Wörter': '911' },
        { Redner: 'Caesare Collins', Thema: 'Kohlesubventionen', Datum: '2012-11-06', 'Wörter': '1119' },
        { Redner: 'Bernhard Belling', Thema: 'Kohlesubventionen', Datum: '2012-11-05', 'Wörter': '1210' },
        { Redner: 'Alexander Abel', Thema: 'Bildungspolitik', Datum: '2012-10-30', 'Wörter': '5310' },
        { Redner: 'Alexander Abel',Thema: 'Innere Sicherheit', Datum: '2012-12-11', 'Wörter': '911' } ]
  
        assert.equal("Alexander Abel", questions.MostSecurity(csvData));
      });
    });
});

describe('Question', function () {
    describe('#MostSecurity()', function () {
      it('should return "null" as clear answer is not available for most speeches on Innere Sicherheit', function () {
  
        const csvData = [ { Redner: 'Alexander Abel', Thema: 'Kohlesubventionen', Datum: '2012-12-11', 'Wörter': '911' },
        { Redner: 'Caesare Collins', Thema: 'Kohlesubventionen', Datum: '2012-11-06', 'Wörter': '1119' },
        { Redner: 'Bernhard Belling', Thema: 'Innere Sicherheit', Datum: '2012-11-05', 'Wörter': '1210' },
        { Redner: 'Alexander Abel', Thema: 'Bildungspolitik', Datum: '2012-10-30', 'Wörter': '5310' },
        { Redner: 'Alexander Abel',Thema: 'Innere Sicherheit', Datum: '2012-12-11', 'Wörter': '911' } ]
  
        assert.equal(null, questions.MostSecurity(csvData));
      });
    });
});

describe('Question', function () {
    describe('#LeastWordy()', function () {
      it('should return "Caesare Collins" for speaker with least words in speeches', function () {
  
        const csvData = [ { Redner: 'Alexander Abel', Thema: 'Innere Sicherheit', Datum: '2012-12-11', 'Wörter': '911' },
        { Redner: 'Caesare Collins', Thema: 'Kohlesubventionen', Datum: '2012-11-06', 'Wörter': '1119' },
        { Redner: 'Bernhard Belling', Thema: 'Kohlesubventionen', Datum: '2012-11-05', 'Wörter': '1210' },
        { Redner: 'Alexander Abel', Thema: 'Bildungspolitik', Datum: '2012-10-30', 'Wörter': '5310' },
        { Redner: 'Alexander Abel',Thema: 'Innere Sicherheit', Datum: '2012-12-11', 'Wörter': '911' } ]
  
        assert.equal("Caesare Collins", questions.LeastWordy(csvData));
      });
    });
});

describe('Question', function () {
    describe('#LeastWordy()', function () {
      it('should return "null" as clear answer is not available for speaker with least words in speeches', function () {
  
        const csvData = [ { Redner: 'Alexander Abel', Thema: 'Innere Sicherheit', Datum: '2012-12-11', 'Wörter': '911' },
        { Redner: 'Caesare Collins', Thema: 'Kohlesubventionen', Datum: '2012-11-06', 'Wörter': '1119' },
        { Redner: 'Bernhard Belling', Thema: 'Kohlesubventionen', Datum: '2012-11-05', 'Wörter': '1119' },
        { Redner: 'Alexander Abel', Thema: 'Bildungspolitik', Datum: '2012-10-30', 'Wörter': '5310' },
        { Redner: 'Alexander Abel',Thema: 'Innere Sicherheit', Datum: '2012-12-11', 'Wörter': '911' } ]
  
        assert.equal(null, questions.LeastWordy(csvData));
      });
    });
});