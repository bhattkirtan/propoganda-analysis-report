var assert = require('assert');
var fetchData = require('../fetchData.jsx');

describe('fetchData', function () {
    describe('#getPoliticalPropogandaData()', function () {
      it('should return 4 records', function (done) {
        var csvData = [["Alexander Abel", "Innere Sicherheit","2013-12-11", "911"],
        ["Caesare Collins", "Kohlesubventionen", "2013-11-06", "1119"],      
        ["Bernhard Belling", "Kohlesubventionen", "2013-11-05", "1210"],
        ["Alexander Abel", "Bildungspolitik","2013-10-30", "5310"]]
        var url = 'https://dev-stefan.s3.amazonaws.com/politics.csv'
        fetchData.getPoliticalPropogandaData(url,function(error, speechesData) {
          assert.notEqual(0,speechesData.length);        
          done();
        })
      });
    });
  });

describe('fetchData', function () {
  describe('#getPoliticalPropogandaData()', function () {
    it('should return 4 records', function (done) {
      var csvData = [["Alexander Abel", "Innere Sicherheit","2013-12-11", "911"],
      ["Caesare Collins", "Kohlesubventionen", "2013-11-06", "1119"],      
      ["Bernhard Belling", "Kohlesubventionen", "2013-11-05", "1210"],
      ["Alexander Abel", "Bildungspolitik","2013-10-30", "5310"]]
      var url = 'https://dev-stefan.s3.amazonaws.com/politics.csv'
      fetchData.getPoliticalPropogandaData(url,function(error, speechesData) {
        assert.equal(csvData.length,speechesData.length);        
        done();
      })
    });
  });
});

describe('fetchData', function () {
    describe('#getPoliticalPropogandaData()', function () {
      it('should return 1 record as "Alexander Abel"', function (done) {
        var csvData = [["Alexander Abel", "Innere Sicherheit","2013-12-11", "911"],
        ["Caesare Collins", "Kohlesubventionen", "2013-11-06", "1119"],      
        ["Bernhard Belling", "Kohlesubventionen", "2013-11-05", "1210"],
        ["Alexander Abel", "Bildungspolitik","2013-10-30", "5310"]]
        var url = 'https://dev-stefan.s3.amazonaws.com/politics.csv'
        fetchData.getPoliticalPropogandaData(url,function(error, speechesData) {
          assert.equal(csvData[0][0],speechesData[0].Redner);        
          done();
        })
      });
    });
  });
