var request = require('supertest');
describe('loading express', function () {
  var server;
  beforeEach(function () {
    server = require('../server');
  });
  afterEach(function () {
    server.close();
  });
  it('responds to /', function testSlash(done) {
  request(server)
    .get('/')
    .expect(200, done);
  });
  it('responds to /evaluation with no url querystring', function testSlash(done) {
    request(server)
      .get('/evaluation')
      .expect(200, done);
    });
    it('responds to /evaluation with valid url', function testSlash(done) {
      request(server)
        .get('/evaluation?url=https://dev-stefan.s3.amazonaws.com/politics.csv&url=https://dev-stefan.s3.amazonaws.com/politics.csv')
        .expect(200, done);
      });
  it('404 everything else', function testPath(done) {
    request(server)
      .get('/invalidURL')
      .expect(404, done);
  });
});