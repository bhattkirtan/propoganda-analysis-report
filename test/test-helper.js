var assert = require('assert');
var questions = require('../helper.jsx');

describe('helper', function () {
  describe('#getSpeakerWithMaxOccurances()', function () {
    it('should return "Alexander Abel" with most occurances', function () {
      const csvData = [ { Redner: 'Alexander Abel', Thema: 'Innere Sicherheit', Datum: '2012-12-11', 'Wörter': '911' },
    { Redner: 'Caesare Collins', Thema: 'Kohlesubventionen', Datum: '2012-11-06', 'Wörter': '1119' },
    { Redner: 'Bernhard Belling', Thema: 'Kohlesubventionen', Datum: '2012-11-05', 'Wörter': '1210' },
    { Redner: 'Alexander Abel', Thema: 'Bildungspolitik', Datum: '2012-10-30', 'Wörter': '5310' },
    { Redner: 'Alexander Abel',Thema: 'Innere Sicherheit', Datum: '2012-12-11', 'Wörter': '911' } ]
      assert.equal("Alexander Abel", questions.getSpeakerWithMaxOccurances(csvData));
    });
  });
});

describe('helper', function () {
  describe('#getSpeakerWithMaxOccurances()', function () {
    it('should return "null" as clear answer is not available', function () {
      const csvData = [ { Redner: 'Alexander Abel', Thema: 'Innere Sicherheit', Datum: '2012-12-11', 'Wörter': '911' },
      { Redner: 'Caesare Collins', Thema: 'Kohlesubventionen', Datum: '2012-11-06', 'Wörter': '1119' },
      { Redner: 'Bernhard Belling', Thema: 'Kohlesubventionen', Datum: '2012-11-05', 'Wörter': '1210' },
      { Redner: 'Caesare Collins', Thema: 'Bildungspolitik', Datum: '2012-10-30', 'Wörter': '5310' },
      { Redner: 'Alexander Abel',Thema: 'Innere Sicherheit', Datum: '2012-12-11', 'Wörter': '911' } ]
      assert.equal(null, questions.getSpeakerWithMaxOccurances(csvData));
    });
  });
});