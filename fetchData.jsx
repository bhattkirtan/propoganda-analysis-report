/*
    currently rows in csv are  read and used directly for processing
    After processing the file output will be in below format. 
    Disadvantage here is that 
        1. column header of each csv file should match exactly 
        2. index needs to be used to access the column for processing
        [["Alexander Abel", "Education Policy", "2012-10-30", "5310"],
        ["Bernhard Belling", "coal subsidies", "2013-11-05", "1210"],
        ["Caesare Collins", "Coal Subsidies", "2012-11-06", "1119"],
        ["Alexander Abel", "Innere Sicherheit", "2012-12-11", "911"]]
  
    Solution coming soon for this    
    For best design csv files should be processed in below format with header info and data in key value pair 
    so that we can avoid indexes and create a better format
        [["Redner":"Alexander Abel", "Thema": "Education Policy", "Datum ": "2012-10-30", "Worter": "5310"],
        ]
*/
const csv = require('csvtojson')
var request = require('request');
module.exports = {
    getPoliticalPropogandaData: function (url, callback) {
        var speechesData = []
        var headerText = []        
        csv()
            .fromStream(request.get(url))
            .on('header',(header)=>{
                if(headerText.lenth == 0)
                headerText.splice(0,0,header)   
            })
            .on('json',(jsonObj, rowIndex)=>{
                speechesData.splice(0, 0, jsonObj)
                //console.log("speechesData ",speechesData)
            })
            .on('done', (error) => {
                // console.log('done');
                callback(error, speechesData,headerText);
            })
    },
}