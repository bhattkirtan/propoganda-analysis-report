module.exports = {
    getSpeakerWithMaxOccurances: function (propogandaData) {
        var max = 0;
        var speakerWithMostSpeeches = null;
        var isClearAnswer = true;

        //console.log("data ",propogandaData)

        // get max occurances of speaker in data
        propogandaData.reduce(function (distinctData, data) {
            // group by speaker name
            distinctData[data.Redner] = (distinctData[data.Redner] || 0) + 1;

            // check if the current count of speech by speaker is greater than max
            if (distinctData[data.Redner] > max) {
                max = distinctData[data.Redner]; // set new count as max value
                speakerWithMostSpeeches = data.Redner; // set new speaker name
                isClearAnswer = true; // mark as clean answer as max value is found
            }
            if (distinctData[data.Redner] == max && data.Redner != speakerWithMostSpeeches) {
                isClearAnswer = false; // mark as not a clean answer as speaker with same speeches identified
            }
            //console.log(data[0], distinctData[data[0]], max, speakerWithMostSpeeches, isClearAnswer)
            return distinctData;
        }, {});

        if (!isClearAnswer) speakerWithMostSpeeches = null; // set null if response is not clear
        return speakerWithMostSpeeches
    },
}