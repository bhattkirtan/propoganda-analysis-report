# Propoganda Analysis Report

### Install

```sh
$ git clone git clone https://gitlab.com/bhattkirtan/propoganda-analysis-report.git # or download zip
$ cd propoganda-analysis-report
$ npm install
```

### Run

```sh
$ npm start
```

### test

```sh
$ npm test
```

### License

MIT

Rahmenbedingungen
Umsetzungsprache ist scala 2.12 (bevorzugt) oder java 1.8.0. Ihre
Losung sollte vollst  ̈ andigen  ̈ !ellcode enthalten, sowie ein script zum Bauen bzw.
ausfuhren der Anwendung. Es gibt keine Einschr  ̈ ankung von verwendbaren Biblio-  ̈
theken.
Zum Test der Anwendung wird eine JVM der version 1.8.0 121-b13 oder
spater verwendet. Die JVM wird folgende Parameter verwenden:  ̈
-Xmx256M -Xms32M
2 Aufgabenstellung: Politische Reden
Ziel der Aufgabe ist die Verarbeitung von Statistiken uber politische Reden. Die An-  ̈
wendung verarbeitet CSV-Dateien (UTF-8 Encoding), die folgendem Schema entsprechen:
Redner, Thema, Datum, W ̈orter
Alexander Abel, Bildungspolitik, 2012-10-30, 5310
Bernhard Belling, Kohlesubventionen, 2012-11-05, 1210
Caesare Collins, Kohlesubventionen, 2012-11-06, 1119
Alexander Abel, Innere Sicherheit, 2012-12-11, 911
Diese Beispieldatei kann von https://dev-stefan.s3.amazonaws.
com/politics.csv heruntergeladen werden.
Die Anwendung stellt einen HTTP-Server zur Verfugung, der 1 oder mehrere  ̈
URLS (h"p und h"ps) als !ery-Parameter unter der Route
GET /evaluation?url=url1&url=url2

entgegennimmt. Die an diesen URLs be#ndlichen CSV-Dateien werden herunterge-
laden, auswertet und folgende Fragen beantwortet:

1. Welcher Politiker hielt im Jahr 2013 die meisten Reden
2. Welcher Politiker hielt die meisten Reden zum $ema ”Innere Sicherheit”
3. Welcher Politiker sprach insgesamt die wenigsten Worter  ̈
Die Ausgabe wird in einer JSON-Struktur ubermi  ̈ "elt. Wenn fur eine Frage keine,  ̈
oder keine eindeutige Antwort moglich ist, soll dieses Feld mit  ̈ null gefullt werden.  ̈
Das Programm wurde f  ̈ ur die oben angegebene Datei folgende Ausgabe liefern:  ̈
{
”mostSpeeches”: null,
”mostSecurity”: ”Alexander Abel”,
”leastWordy”: ”Caesare Collins”
}