/*  This file contains the business logic for the list of question ask in requirements 
    one set of questions worked by one developer. Different developer can work on different business requrirements
    example A set of questions, B set of questions and this can be combined later for execution
*/
var helper = require('./helper.jsx');

module.exports = {
    answersFor: function (text) {
        var myQuestions = [
            { name: 'MostSpeeches', returnVal: null },
            { name: 'MostSecurity', returnVal: null },
            { name: 'LeastWordy', returnVal: null }];
        return myQuestions
    },

    MostSpeeches: function (propogandaData, dataHeader, filterCriteria) {

        //currently index is hardcoded. this can be mapped from header data.  

        // filter by year 
        propogandaData = propogandaData.filter(x => x.Datum.includes("2013"));
        //console.log(" propogandaData ",propogandaData)
        // get speaker with max occurances
        return helper.getSpeakerWithMaxOccurances(propogandaData);
    },

    MostSecurity: function (propogandaData, dataHeader, filterCriteria) {

        // filter by topic 
        propogandaData = propogandaData.filter(x => x.Thema == "Innere Sicherheit");
        //console.log(" propogandaData ",propogandaData)
        // get speaker with max occurances
        return helper.getSpeakerWithMaxOccurances(propogandaData);
    },

    LeastWordy: function (propogandaData,dataHeader) {
        var max = 0;
        var speakerWithLeastWords = null;
        var isClearAnswer = true;

        //console.log("data ",propogandaData)

        // aggregate words in speech
        var aggregatedData = propogandaData.reduce(function (distinctData, data) {  
            distinctData[data.Redner] = (distinctData[data.Redner] || 0) + parseFloat(data.Wörter);
            return distinctData;
        }, {});

        // get minimum words count from value 
        var minWord = Math.min.apply(Math, Object.values(aggregatedData))
        
        // get speaker name with least words
        var result = Object.keys(aggregatedData).reduce((result, key) => {
            if (aggregatedData[key] === minWord) 
                result.push(key);
            return result;
        }, []);

        //console.log("returnVal ", Object.values(aggregatedData), aggregatedData, minWord, result)

        if (result.length != 1) return null; // set null if response is not clear
        return result[0]
    },
}